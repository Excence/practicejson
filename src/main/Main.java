package main;

public class Main{
    public static void main(String[] args) {
        Person p = new Person("Иван","Иванов", 33);

        System.out.println(p.parseToString());
        System.out.println(p.toString());

        String str = "{\"name\":\"Петр\",\"surname\":\"Петров\",\"age\":20}";

        Person p2 = p.parseToPerson(str);
        System.out.println(p2.toString());

        p2.parseToFile(str);
        System.out.println(p2.readFromFile());

    }
}
