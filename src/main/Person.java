package main;

import java.io.*;
import java.util.Arrays;

public class Person {
    private String name;
    private String surname;
    private int age;


    public Person(){}

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;

    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }

    public String parseToString(){
        return "{\"name\":\""+this.name+"\",\"surname\":\""+this.surname+"\",\"age\":"+this.age+"}";
    }

    public Person parseToPerson(String str){
        String[] buf = firstSplit(str);

        String name = secondSplit(buf[0]).replace("\"","");
        String surname = secondSplit(buf[1]).replace("\"","");
        int age = Integer.parseInt(secondSplit(buf[2]).replace("}",""));

        return  new Person(name, surname, age);
    }

    private String[] firstSplit(String str){
        String[] buf = new String[3];
        for (String s: buf) {
            buf = str.split(",");
        }
        return  buf;
    }

    private String secondSplit(String str){
        String[] buf = new String[2];
        for (String s: buf) {
            buf = str.split(":");
        }
        return buf[1];
    }

    public File parseToFile(String str){
        File file = new File("info");
        if (!file.exists()){
            try {
                file.createNewFile();
                writeToFile(file, str);
                return file;
            } catch (Exception ex){
                System.out.println("Файл info не создан");
            }
        } else {
            writeToFile(file, str);
            return file;
        }
        return null;
    }

    private void writeToFile(File file, String str) {
        try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            String[] buf = firstSplit(str);
            bufferedWriter.write("имя : " + secondSplit(buf[0]).replace("\"","") +
                                    "; фамилим : " +secondSplit(buf[1]).replace("\"","") +
                                    "; возраст : " + secondSplit(buf[2]).replace("}",""));
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception ex){
            System.out.println("Запись в файл info не удалась");
        }
    }

    public String readFromFile(){
        File file = new File("info");
        try {
            FileReader fileReader =new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String str = new String();
            while (bufferedReader.ready()){
                str += bufferedReader.readLine() + " ";
            }
            bufferedReader.close();
            return str;
        } catch (Exception ex){
            return "Что-то не так с чтением";
        }
    }

}
